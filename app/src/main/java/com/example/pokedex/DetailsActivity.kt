package com.example.pokedex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.pokedex.Model.Pokemon
import com.example.pokedex.Retrofit.list
import com.example.pokedex.Retrofit.listRetriver
import kotlinx.android.synthetic.main.activity_details.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        var textName = findViewById<TextView>(R.id.name)
        val textNum = findViewById<TextView>(R.id.num)
        val textHeight = findViewById<TextView>(R.id.num)
        val textWeight = findViewById<TextView>(R.id.num)

        val retriver = listRetriver()
        val callback = object : Callback<list>{

            override fun onFailure(call: Call<list>, t: Throwable) {
            }
            override fun onResponse(call: Call<list>, response: Response<list>) {

                val pokemonList = response.body()?.pokemon
                val position = getIntent().getIntExtra("position", 0)
                val myPokemon = pokemonList?.get(position)

                setDetailPokemon(myPokemon)
            }
            fun setDetailPokemon (pokemon: Pokemon?) {

                textName.text = pokemon?.name
                textNum.text = pokemon?.num
                textHeight.text = pokemon?.height
                textWeight.text = pokemon?.weight
            }

        }
        retriver.getList(callback)
    }
}
