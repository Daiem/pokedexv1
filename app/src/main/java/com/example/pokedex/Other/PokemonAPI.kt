package com.example.pokedex.Other

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface PokemonAPI {
    @GET("Biuni/PokemonGO-Pokedex/master/pokedex.json")
    fun getList() : Call<list>
}
class  list( val pokemon: List<listResults>)
class  listResults( val name: String,
                    val img: String,
                    val num: String)

class listRetriver {
    private val service : PokemonAPI

    init {
        val retrofit = Retrofit.Builder().baseUrl("https://raw.githubusercontent.com/").addConverterFactory(GsonConverterFactory.create()).build()
        service = retrofit.create(PokemonAPI::class.java)
    }

    fun getList(callback: Callback<list>){
        val call = service.getList()
        call.enqueue(callback)
    }
}
